extern crate xml;
extern crate cairo;
extern crate librsvg;

use librsvg::CairoRenderer;
use std::fs::File;
use std::io::BufReader;

use xml::reader::{EventReader, XmlEvent};

#[derive(Debug, Clone)]
pub struct Coord {
    x: f64,
    y: f64,
    width: f64,
    height: f64,
    name: String,
}

fn main() {
    let group_name = "slides";
    let path = "slides.svg";
    let file = File::open(path).unwrap();
    let file = BufReader::new(file);

    let parser = EventReader::new(file);
    let mut found = false;
    let mut slides: Vec<Coord> = vec!();
    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, attributes, .. }) => {
                let local = xml::name::OwnedName::local("id");
                let value = xml::attribute::OwnedAttribute::new(local, group_name);
                if name.local_name == "g" {
                    for a in attributes.clone() {
                        if a == value {
                            found = true;
                            println!("Group {} was found", group_name);
                        }
                    }
                }
                if found && name.local_name == "rect" {
                    // we need x and y form attributes
                    let x = xml::name::OwnedName::local("x");
                    let y = xml::name::OwnedName::local("y");
                    let width = xml::name::OwnedName::local("width");
                    let height = xml::name::OwnedName::local("height");
                    let name = xml::name::OwnedName::local("id");
                    let mut value = Coord {name: String::from(""), x : 0f64, y : 0f64, width : 0f64, height : 0f64};
                    //361.421 inkscape width for slides group
                    //1280 rsvg width for slides group
                    for a in attributes {
                        if a.name == name {
                            value.name = a.value.clone();
                        }
                        if a.name == x {
                            value.x = (a.value.parse::<f64>().unwrap() * 3.5416).round();
                        }
                        if a.name == y {
                            value.y = (a.value.parse::<f64>().unwrap() * 3.5416).round();
                        }
                        if a.name == width {
                            value.width = (a.value.parse::<f64>().unwrap() * 3.5416).round();
                        }
                        if a.name == height {
                            value.height = (a.value.parse::<f64>().unwrap() * 3.5416).round();
                        }
                    }
                    slides.push(value);
                }
            }
            Ok(XmlEvent::EndElement { name }) => {
                if found && name.local_name.contains("g") {
                    break;
                }
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }

    slides.sort_by(|a, b| a.y.partial_cmp(&b.y).unwrap());
    let handle = librsvg::Loader::new().read_path(path).unwrap();
    let renderer = librsvg::CairoRenderer::new(&handle);

    for (index, current) in slides.iter().enumerate() {
        create_pdf(&renderer, index, format!("#{}",current.name));
    }
    // TODO: merge each single pdf
    // ./stapler/stapler zip `ls output*.pdf` merged.pdf
}

fn create_pdf(handle: &CairoRenderer, index: usize, name: String) -> Option<()> {
    let doc_width = handle.intrinsic_dimensions ().width.unwrap ().length;
    let doc_height = handle.intrinsic_dimensions ().height.unwrap ().length;

    // Get the correct viewport
    let viewport = cairo::Rectangle {x: 0., y: 0., width: doc_width, height: doc_height};
    let (_, rect) = handle.geometry_for_layer(Some(name.as_str()), &viewport).unwrap();

    let pdf_surface = cairo::PdfSurface::new(rect.width, rect.height, format!("./output{:06}.pdf", index + 1));
    let cr = cairo::Context::new(&pdf_surface);
    handle.render_document(&cr,    
                           &cairo::Rectangle {    
                               x: -rect.x,    
                               y: -rect.y,    
                               width: doc_width,                       
                               height: doc_height,                        
                           }    
    ).unwrap ();

    return None;
}
