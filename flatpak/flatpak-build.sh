#!/bin/bash

#flatpak --user install flathub org.freedesktop.Sdk.Extension.rust-stable
flatpak-builder --repo=repo svago-export org.gnome.SvagoExport.json --force-clean

flatpak --user remote-add --no-gpg-verify --if-not-exists svago-export-repo repo
flatpak --user install svago-export-repo org.gnome.SvagoExport

#flatpak-builder --run fractal org.gnome.SvagoExport-local.json fractal
